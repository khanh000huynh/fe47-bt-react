import React from "react";
import "./App.css";
import Product from "./Product";
// import Glasses from "./Glasses";

function App() {
  return (
    <div className="App">
      <Product />
      {/* <Glasses /> */}
    </div>
  );
}

export default App;
