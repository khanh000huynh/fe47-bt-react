import React, { Component } from "react";
import "./style.css";

let arrProduct = [
  {
    id: 1,
    price: 30,
    name: "GUCCI G8850U",
    url: "./glassesImage/v1.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 2,
    price: 50,
    name: "GUCCI G8759H",
    url: "./glassesImage/v2.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 3,
    price: 30,
    name: "DIOR D6700HQ",
    url: "./glassesImage/v3.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 4,
    price: 30,
    name: "DIOR D6005U",
    url: "./glassesImage/v4.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 5,
    price: 30,
    name: "PRADA P8750",
    url: "./glassesImage/v5.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 6,
    price: 30,
    name: "PRADA P9700",
    url: "./glassesImage/v6.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 7,
    price: 30,
    name: "FENDI F8750",
    url: "./glassesImage/v7.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 8,
    price: 30,
    name: "FENDI F8500",
    url: "./glassesImage/v8.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },

  {
    id: 9,
    price: 30,
    name: "FENDI F4300",
    url: "./glassesImage/v9.png",
    desc:
      "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
  },
];

class Glasses extends Component {
  state = {
    name: arrProduct[0].name,
    description: arrProduct[0].desc,
    imageUrl: arrProduct[0].url,
  };

  updateGlasses = (glasses) => () => {
    this.setState({
      name: glasses.name,
      description: glasses.desc,
      imageUrl: glasses.url,
    });
  };

  renderGlasses = () => {
    return arrProduct.map((glasses, index) => {
      return (
        <div key={index}>
          <img
            src={glasses.url}
            alt="glasses"
            onClick={this.updateGlasses(glasses)}
          />
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        <header>
          <h1>Try your favorite glasses</h1>
        </header>
        <section id="model">
          <img src="./glassesImage/model.jpg" alt="model" />
          <img src="./glassesImage/model.jpg" alt="model" />
          <img src={this.state.imageUrl} alt="glasses" />
          <div className="glass-description">
            <h3>{this.state.name}</h3>
            <p>{this.state.description}</p>
          </div>
        </section>
        <section id="glasses">{this.renderGlasses()}</section>
      </div>
    );
  }
}

export default Glasses;
