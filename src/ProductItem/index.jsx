import React, { Component } from "react";

class ProductItem extends Component {
  render() {
    const { name, img, desc } = this.props.product;
    return (
      <div>
        <img src={img} alt="product1" />
        <h3>{name}</h3>
        <p>{desc}</p>
        <button>Detail</button>
        <button>+ Cart</button>
      </div>
    );
  }
}

export default ProductItem;
